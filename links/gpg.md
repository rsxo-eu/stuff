# Links zum Thema gpg

## Nitrokey

- [https://docs.nitrokey.com/de/index.html](https://docs.nitrokey.com/de/index.html)

	Hinweis für die Verwendung des Nitrokey Pro unter Linux: Nicht vergessen die ```libccid``` zu installieren! :-)

- [https://www.kuketz-blog.de/zwei-schluessel-fuer-alle-faelle-nitrokey-teil1/](https://www.kuketz-blog.de/zwei-schluessel-fuer-alle-faelle-nitrokey-teil1/ "kuketz-blog: Zwei Schlüssel für alle Fälle – Nitrokey Teil1")

## Schlüssel Verifikation

- [Mein keyoxide Profil](https://keyoxide.org/9A45912BD62D8415885A862E61B3B0E3119A0792)

- [https://docs.keyoxide.org](https://docs.keyoxide.org)

### Profilbild in gpg public Key hinzufügen
```
gpg --edit-key <your key id>
```
Mit ```addphoto``` kann dann das Bild ausgewählt werden. Hinweins: Das Foto wird in den pubkey abgelegt.
Je größer das Bild, desto größer wird auch die pubkey-Datei!
Das Foto muss im JPG-Format vorliegen. Eine gute Bildgröße soll ```240x288``` sein.

