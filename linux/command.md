# Linux-Befehle

## Suche in gz-File
```
zgrep 'your-text-here' /path/file.gz
```
Falls es zu viele Einträge gibt:
```
zgrep 'your-text-here' /path/file.gz | more
```

## Suche in allen gz-Dateien im Ordner

```
find . -name \*.log.gz -print0 | xargs -0 zgrep "your-text-here"
```

