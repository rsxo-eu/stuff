# git Konfiguration

## Default branch festlegen
```
git config --global init.defaultBranch main
```

## Alias in ~/.gitconfig
```
[alias]
	st = status
	co = checkout
        br = branch
	lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
	nfm = merge --no-ff
	ffm = merge --ff-only
	ci = commit
	ss = status -s
	aa = !git add -u && git add . && git status
	ap = add -p
	df = diff
	lg = log --graph --pretty=format:'%C(bold red)%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold green)<%an>%Creset' --abbrev-commit --date=relative
	cm = commit --verbose
	ca = commit -a --verbose
	am = commit --amend
	ph = push
	ft = fetch
	pl = pull
	pr = pull --rebase
	fft = merge --ff-only @{u}
	cis = commit -S
```

## Commits signieren
Festlegen von Benutzernamen, eMail und PGP Signierschlüssel
```
git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "MY_NAME@example.com"
git config --global user.signingkey XXXXXXXXX
```

Signierung aktivieren
```
git config --global commit.gpgsign true
```

Commits gpg Signatur anzeigen
```
git log --show-signature
```

