# Infos zu RUST

## Rust Handbuch
[https://rust-lang-de.github.io/rustbook-de/](https://rust-lang-de.github.io/rustbook-de/)

Neues Projekt mit cargo erstellen
```
cargo new hello_cargo
```

Projekt bauen mit cargo
```
cargo build
```

Projekt freigeben (release)
```
cargo build --release
```

